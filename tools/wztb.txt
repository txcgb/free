--by 易富焙
    function 创建文件夹(路径)
     File(路径).mkdirs()
    end
    tpic=TXCGB.."pic/"

全屏框架=
{
  LinearLayout,
  orientation='vertical',
  layout_width='fill',
  layout_height='fill',
  background='#00FFFFFF',
  {
    LinearLayout,
    layout_marginTop='8%w';
    layout_margin='5%w';
    layout_gravity='center';
    orientation='vertical',
    layout_width='50%w',
    layout_height='50%w',
    
    {
      CardView;
      layout_gravity='center';
      layout_width='fill';
      layout_height='fill';
      CardBackgroundColor='#ff1e8ee8';
      {
        TextView;
        layout_gravity='center';
        gravity='center';
        textColor='#ffffffff';
        text='Free';
        textSize='20';
        id="字";
      };
      id="卡";
    };

  };
  {
    CardView;
    radius="8dp";
    layout_width="match_parent";
    Elevation="0dp";
    layout_margin="16dp";
    layout_marginTop="0dp";
    {
      LinearLayout;
      Orientation='vertical';
      layout_width='fill';
      layout_height='fill';
      {
        FrameLayout;
        layout_width="fill";
        {
          LinearLayout;
          Orientation='horizontal';
          layout_width='fill';
          {
            TextView;
            
            text='圆角弧度';
            layout_marginLeft='18dp';
            layout_margin='8dp';
            id="宽度调整显示";
          };
        };

      };
      {
        SeekBar;
        layout_margin='8dp';
        layout_marginTop="0dp";
        id='圆角弧度';
        layout_width='fill';
        
      },
    },
  },
  {
    CardView;
    radius="8dp";
    layout_width="match_parent";
    Elevation="0dp";
    layout_margin="16dp";
    layout_marginTop="0dp";
    {
      LinearLayout;
      Orientation='vertical';
      layout_width='fill';
      layout_height='fill';
      {
        FrameLayout;
        layout_width="fill";
        {
          LinearLayout;
          Orientation='horizontal';
          layout_width='fill';
          {
            TextView;
            
            text='内容范围';
            layout_marginLeft='18dp';
            layout_margin='8dp';
            id="宽度调整显示";
          };
        };

      };
      {
        SeekBar;
        layout_margin='8dp';
        layout_marginTop="0dp";
        id='内容范围';
        layout_width='fill';
        
      },
    },
  },
  {
    LinearLayout,
    orientation='horizontal',
    layout_width='fill',
    layout_height='wrap',
    background='#00FFFFFF',
    {
      Button;
      id="文字";
      layout_width='40%w';
      layout_margin='5%w';
      layout_gravity="center";
      text="设置文字";

    };
    {
      Button;
      id="字颜色";
      layout_width='40%w';
      layout_margin='5%w';
      layout_gravity="center";
      text="设置文字颜色";
      

      
    };
  };

  {
    LinearLayout,
    orientation='horizontal',
    layout_width='fill',
    layout_marginTop='-8%w';
    layout_height='wrap',
    background='#00FFFFFF',
    {
      Button;
      id="背景色";
      layout_width='40%w';
      layout_margin='5%w';
      layout_gravity="center";
      text="设置背景色";
      onClick=function()

      end
    };
    {
      Button;
      id="保存";
      layout_width='40%w';
      layout_margin='5%w';
      layout_gravity="center";
      text="保存图片";
    };
  };

};
activity.setContentView(loadlayout(全屏框架))
function 修改()
  
  卡.radius=圆角;
end
function SavePicture(id)
  import "java.io.FileOutputStream"
  import "java.io.File"
  import "android.graphics.Bitmap"
  id.setDrawingCacheEnabled(true)
  local obmp = Bitmap.createBitmap(id.getDrawingCache());
  if obmp then
    创建文件夹(tpic)
    local filepath =tpic..os.time()..".png"
    local f = File(tostring(filepath))
    local out = FileOutputStream(f)
    obmp.compress(Bitmap.CompressFormat.PNG,95,out)
    out.flush()
    out.close()
    id.setDrawingCacheEnabled(false)
    import "android.media.MediaScannerConnection"
    import "java.io.File"
    MediaScannerConnection.scanFile(activity,{File(filepath).getAbsolutePath()}, nil, nil)
    function 提示消息(文本)
      Toast.makeText(activity,文本,Toast.LENGTH_SHORT).show()
    end
    
    print("已保存至:"..filepath)
    return true
   else
    
    function 提示消息(文本)
      Toast.makeText(activity,文本,Toast.LENGTH_SHORT).show()
    end
    
    print("保存失败")
    return false
  end
end

保存.onClick=function()
  SavePicture(卡)
end
文字.onClick=function()
  InputLayout={
    LinearLayout;
    orientation="vertical";
    Focusable=true,
    FocusableInTouchMode=true,
    {
      EditText;
      hint="输入";
      layout_marginTop="5dp";
      layout_width="80%w";
      layout_gravity="center",
      id="edit";
    };
  };
  AlertDialog.Builder(this)
  .setTitle("设置文字")
  .setView(loadlayout(InputLayout))
  .setPositiveButton("确定",{onClick=function(v)
      if #edit.text==0 then
        print("没有内容")
      else
        字.Text=edit.text
      end
    end})
  .setNegativeButton("取消",nil)
  .show()
end





function onResult(a,b)
  if xz == "wen" then
    字.setTextColor(tonumber(b))
    xz = nil
  elseif xz == "bei" then
    卡.setBackgroundColor(tonumber(b))
    xz = nil
  end
end

背景色.onClick=function()

  import "android.widget.TextView"
  import "android.app.AlertDialog"
  import "android.graphics.PorterDuff"
  import "android.content.Context"
  
  import "android.widget.SeekBar"
  import "android.os.Build"
  import "android.graphics.PorterDuffColorFilter"
  import "android.widget.LinearLayout"


  function 拖动条颜色(id,颜色)
    id.ProgressDrawable.setColorFilter(PorterDuffColorFilter(颜色,PorterDuff.Mode.SRC_ATOP))
    
    id.Thumb.setColorFilter(PorterDuffColorFilter(颜色,PorterDuff.Mode.SRC_ATOP))
  end
  
  function 对话框按钮颜色(id,button,颜色)
    if Build.VERSION.SDK_INT >= 22 then
      import "android.graphics.PorterDuffColorFilter"
      import "android.graphics.PorterDuff"
      if button==1 then
        id.getButton(id.BUTTON_POSITIVE).setTextColor(颜色)
      elseif button==2 then
        id.getButton(id.BUTTON_NEGATIVE).setTextColor(颜色)
      elseif button==3 then
        id.getButton(id.BUTTON_NEUTRAL).setTextColor(颜色)
      end
    end
  end
  主色=0xff1e8ae8;
  副色=0xFFd68189;
  文字色=0xffffffff;
  警告色=0xff60c5ba;
  背景底层色=0xfff1f1f1;
  背景顶层色=0xffffffff;
  左侧栏项目色=0xFFa3a1a1;
  对话框字体色=0xff000000;
  

  function 颜色选择器()
    require "import"
    import "android.widget.*"
    import "android.view.*"
    import "android.graphics.PorterDuffColorFilter"
    import "android.graphics.PorterDuff"
    取色器=
    {
      LinearLayout;
      orientation="vertical";
      layout_width="fill";
      layout_height="fill";
      gravity="center";
      {
        CardView;
        id="卡片图";
        layout_margin="10dp";
        radius="40dp",
        elevation="0dp",
        layout_width="20%w";
        layout_height="20%w";
      };
      {
        TextView;
        layout_margin="0dp";
        textSize="12sp";
        id="颜色文本";
        textColor=左侧栏项目色;
      };
      {
        SeekBar;
        id="拖动一";
        layout_margin="15dp";
        layout_width="match";
        layout_height="wrap";
      };
      {
        SeekBar;
        id="拖动二";
        layout_margin="15dp";
        layout_width="match";
        layout_height="wrap";
      };
      {
        SeekBar;
        id="拖动三";
        layout_margin="15dp";
        layout_width="match";
        layout_height="wrap";
      };
      {
        SeekBar;
        id="拖动四";
        layout_margin="15dp";
        layout_width="match";
        layout_height="wrap";
      };
    };
    
    local 取色器=loadlayout(取色器)
    拖动一.setMax(255)
    拖动二.setMax(255)
    拖动三.setMax(255)
    拖动四.setMax(255)
    拖动一.setProgress(0xff)
    拖动二.setProgress(0x1e)
    拖动三.setProgress(0x8a)
    拖动四.setProgress(0xe8)
    
    拖动一.setOnSeekBarChangeListener{
      onProgressChanged=function(view, i)
        updateArgb()
      end
    }

    拖动二.setOnSeekBarChangeListener{
      onProgressChanged=function(view, i)
        updateArgb()
      end
    }

    拖动三.setOnSeekBarChangeListener{
      onProgressChanged=function(view, i)
        updateArgb()
      end
    }

    拖动四.setOnSeekBarChangeListener{
      onProgressChanged=function(view, i)
        updateArgb()
      end
    }
    
    function updateArgb()
      local a=拖动一.getProgress()
      local r=拖动二.getProgress()
      local g=拖动三.getProgress()
      local b=拖动四.getProgress()
      local argb_hex=(a<<24|r<<16|g<<8|b)
      颜色文本.Text=string.format("%#x", argb_hex)
      卡片图.setCardBackgroundColor(argb_hex)
    end
    
    argbBuild=AlertDialog.Builder(activity)
    argbBuild.setView(取色器)
    argbBuild.setTitle("选色器")
    argbBuild.setPositiveButton("选择", {
      onClick=function(view)
        argbDialog.cancel()
        卡.setCardBackgroundColor(tonumber(颜色文本.Text ))

      end
    })
    argbBuild.setNeutralButton("复制",{onClick=function()
        local a=拖动一.getProgress()
        local r=拖动二.getProgress()
        local g=拖动三.getProgress()
        local b=拖动四.getProgress()
        local argb_hex=(a<<24|r<<16|g<<8|b)
        local argb_str=string.format("%#x", argb_hex)
        activity.getSystemService(Context.CLIPBOARD_SERVICE).setText(argb_str)
        print("已复制到剪贴板") end})
    
    argbDialog=argbBuild.create()
    argbDialog.setCanceledOnTouchOutside(false)
    function showArgbDialog()
      
      argbDialog.show()
      
      updateArgb()
    end
    showArgbDialog()
    对话框按钮颜色(argbDialog,1,副色)
    对话框按钮颜色(argbDialog,2,副色)
    对话框按钮颜色(argbDialog,3,副色)
    拖动条颜色(拖动一,0xff000000)
    拖动条颜色(拖动二,0xffff0000)
    拖动条颜色(拖动三,0xFF1DE9B6)
    拖动条颜色(拖动四,0xFF6699FF)
  end
  颜色选择器()

end

字颜色.onClick=function()

  import "android.widget.TextView"
  import "android.app.AlertDialog"
  import "android.graphics.PorterDuff"
  import "android.content.Context"
  
  import "android.widget.SeekBar"
  import "android.os.Build"
  import "android.graphics.PorterDuffColorFilter"
  import "android.widget.LinearLayout"


  function 拖动条颜色(id,颜色)
    id.ProgressDrawable.setColorFilter(PorterDuffColorFilter(颜色,PorterDuff.Mode.SRC_ATOP))
    
    id.Thumb.setColorFilter(PorterDuffColorFilter(颜色,PorterDuff.Mode.SRC_ATOP))
  end
  
  function 对话框按钮颜色(id,button,颜色)
    if Build.VERSION.SDK_INT >= 22 then
      import "android.graphics.PorterDuffColorFilter"
      import "android.graphics.PorterDuff"
      if button==1 then
        id.getButton(id.BUTTON_POSITIVE).setTextColor(颜色)
      elseif button==2 then
        id.getButton(id.BUTTON_NEGATIVE).setTextColor(颜色)
      elseif button==3 then
        id.getButton(id.BUTTON_NEUTRAL).setTextColor(颜色)
      end
    end
  end
  主色=0xff1e8ae8;
  副色=0xFFd68189;
  文字色=0xffffffff;
  警告色=0xff60c5ba;
  背景底层色=0xfff1f1f1;
  背景顶层色=0xffffffff;
  左侧栏项目色=0xFFa3a1a1;
  对话框字体色=0xff000000;
  

  function 颜色选择器()
    require "import"
    import "android.widget.*"
    import "android.view.*"
    import "android.graphics.PorterDuffColorFilter"
    import "android.graphics.PorterDuff"
    取色器=
    {
      LinearLayout;
      orientation="vertical";
      layout_width="fill";
      layout_height="fill";
      gravity="center";
      {
        CardView;
        id="卡片图";
        layout_margin="10dp";
        radius="40dp",
        elevation="0dp",
        layout_width="20%w";
        layout_height="20%w";
      };
      {
        TextView;
        layout_margin="0dp";
        textSize="12sp";
        id="颜色文本";
        textColor=左侧栏项目色;
      };
      {
        SeekBar;
        id="拖动一";
        layout_margin="15dp";
        layout_width="match";
        layout_height="wrap";
      };
      {
        SeekBar;
        id="拖动二";
        layout_margin="15dp";
        layout_width="match";
        layout_height="wrap";
      };
      {
        SeekBar;
        id="拖动三";
        layout_margin="15dp";
        layout_width="match";
        layout_height="wrap";
      };
      {
        SeekBar;
        id="拖动四";
        layout_margin="15dp";
        layout_width="match";
        layout_height="wrap";
      };
    };
    
    local 取色器=loadlayout(取色器)
    拖动一.setMax(255)
    拖动二.setMax(255)
    拖动三.setMax(255)
    拖动四.setMax(255)
    拖动一.setProgress(0xff)
    拖动二.setProgress(0x1e)
    拖动三.setProgress(0x8a)
    拖动四.setProgress(0xe8)
    
    拖动一.setOnSeekBarChangeListener{
      onProgressChanged=function(view, i)
        updateArgb()
      end
    }

    拖动二.setOnSeekBarChangeListener{
      onProgressChanged=function(view, i)
        updateArgb()
      end
    }

    拖动三.setOnSeekBarChangeListener{
      onProgressChanged=function(view, i)
        updateArgb()
      end
    }

    拖动四.setOnSeekBarChangeListener{
      onProgressChanged=function(view, i)
        updateArgb()
      end
    }
    
    function updateArgb()
      local a=拖动一.getProgress()
      local r=拖动二.getProgress()
      local g=拖动三.getProgress()
      local b=拖动四.getProgress()
      local argb_hex=(a<<24|r<<16|g<<8|b)
      颜色文本.Text=string.format("%#x", argb_hex)
      卡片图.setCardBackgroundColor(argb_hex)
    end
    
    argbBuild=AlertDialog.Builder(activity)
    argbBuild.setView(取色器)
    argbBuild.setTitle("选色器")
    argbBuild.setPositiveButton("选择", {
      onClick=function(view)
        argbDialog.cancel()
        字.setTextColor(tonumber(颜色文本.Text ))

      end
    })
    argbBuild.setNeutralButton("复制",{onClick=function()
        local a=拖动一.getProgress()
        local r=拖动二.getProgress()
        local g=拖动三.getProgress()
        local b=拖动四.getProgress()
        local argb_hex=(a<<24|r<<16|g<<8|b)
        local argb_str=string.format("%#x", argb_hex)
        activity.getSystemService(Context.CLIPBOARD_SERVICE).setText(argb_str)
        print("已复制到剪贴板") end})
    
    argbDialog=argbBuild.create()
    argbDialog.setCanceledOnTouchOutside(false)
    function showArgbDialog()
      
      argbDialog.show()
      
      updateArgb()
    end
    showArgbDialog()
    对话框按钮颜色(argbDialog,1,副色)
    对话框按钮颜色(argbDialog,2,副色)
    对话框按钮颜色(argbDialog,3,副色)
    拖动条颜色(拖动一,0xff000000)
    拖动条颜色(拖动二,0xffff0000)
    拖动条颜色(拖动三,0xFF1DE9B6)
    拖动条颜色(拖动四,0xFF6699FF)
  end
  颜色选择器()
end


圆角弧度.setMax(300)
圆角弧度.setProgress(40)

内容范围.setMax(200)
内容范围.setProgress(40)
内容=10



圆角弧度.setOnSeekBarChangeListener{
  onStartTrackingTouch=function()
    
  end,
  onProgressChanged=function()
    圆角=圆角弧度.getProgress()+1
    修改()
  end,
  onStopTrackingTouch=function()
    
  end,}
内容范围.setOnSeekBarChangeListener{
  onStartTrackingTouch=function()
    
  end,
  onProgressChanged=function(a)
    
    字.setTextSize(a.Progress)
  end,
  onStopTrackingTouch=function()
    
  end,}

