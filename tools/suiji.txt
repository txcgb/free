--随机数
layout={
 LinearLayout,
 orientation="vertical",
 layout_width="fill",

 {
 LinearLayout,
 layout_width="fill",
 padding="16dp",
 paddingTop="8dp",
 paddingBottom="8dp",
 gravity="center",
 {
 EditText,
 gravity="center",
 ems=6,
 singleLine=true,
 layout_marginTop='30dp';--布局顶距
 backgroundColor=0,
 hint="最小值",
 inputType="number",
 id="min",
 text="0",
 layout_weight=1,
 },
 {
 TextView,
 layout_gravity="center",
 layout_marginTop='30dp';--布局顶距
 text="<",
 },
 {
 EditText,
 ems=6,
 gravity="center",
 layout_marginTop='30dp';--布局顶距
 backgroundColor=0,
 hint="最大值",
 text="35",
 inputType="number",
 layout_weight=1,
 id="max",
 singleLine=true,
 },
 },
 {
 ScrollView,
 layout_width="fill",
 layout_weight=1,
 {
 TextView,
 id="tex",
 textSize="56dp",
 layout_gravity="center",
 layout_height="fill",
 padding="16dp",
 textIsSelectable=true,
 },
 },
 {
 Button,
 text="生成",
 layout_width="fill",
 layout_marginBottom='450dp';--布局底距
 layout_margin="16dp",
 textColor=0xfffafafa,
 id="bt",
 textSize="16dp",
 },
};

require "import"
import "android.app.*"
import "android.os.*"
import "android.widget.*"
import "android.view.*"


activity.setTheme(android.R.style.Theme_DeviceDefault_Light)--设置md主题
--activity.setTheme(R.AndLua1)
activity.setTitle("随机数生成")--标题
activity.setContentView(loadlayout(layout))

--控件圆角函数
function 控件圆角(id,InsideColor,radiu)
 import "android.graphics.drawable.GradientDrawable"
 drawable = GradientDrawable()
 drawable.setShape(GradientDrawable.RECTANGLE)
 drawable.setColor(InsideColor) drawable.setCornerRadii({radiu,radiu,radiu,radiu,radiu,radiu,radiu,radiu});
 id.setBackgroundDrawable(drawable)
end

控件圆角(bt,0xFF2196F3,360)--设置按钮圆角
--来着快乐程序，代码手册首次整理https://www.lanzous.com/i82ncaf
tex.setText(tostring(math.random(0,35)))--设置默认数值为1-35@的随机数值

bt.onClick=function()--按钮点击事件
 tex.setText(tostring(math.random(tonumber(min.text),tonumber(max.text))))--获取输入框的数值并转换为number，取随机数后转换为string型数值并设置给tex
end
