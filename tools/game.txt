--游戏大厅代码
--竖屏
activity.setRequestedOrientation(1);
读取颜色(随机色,随机色1)

layout=--常规框架
{
 LinearLayout;--线性控件
 orientation='vertical';--布局方向
 layout_width='fill';--布局宽度
 layout_height='fill';--布局高度
 background='#ffffff';--布局背景
 {
 TextView;--文本控件
 text='游戏大厅';--显示文字
 textSize='20sp';--文字大小
 textColor='#333333';--文字颜色
 layout_gravity="center|bottom";
 };
 {
 TextView;--横向分割线
 layout_width='95%w';--分割线宽度
 layout_height='5px';--分割线厚度
 layout_marginTop='10dp';--布局顶距
 layout_marginBottom='10dp';--布局底距
 layout_gravity='center';--高度居中
 backgroundColor='#FFBEBEBE';--分割线颜色
 };
 {
 GridView;
 id="list";
 numColumns=3;--列数
 layout_width='fill';--宽度
 layout_height='fill';--高度
 horizontalSpacing="6dp";--横向布局
 verticalSpacing="0dp";--竖向布局
 layout_gravity="center";
 OverScrollMode=2;
 };
}
webView.addView(loadlayout(layout))

function 布局边框(边框粗细,边框颜色,背景颜色,圆角大小)
 import "android.graphics.drawable.GradientDrawable"
 drawable=GradientDrawable()
 drawable.setShape(GradientDrawable.RECTANGLE)
 drawable.setStroke(边框粗细,tonumber(边框颜色))--边框粗细和颜色
 drawable.setColor(tonumber(背景颜色))--背景颜色
 drawable.setCornerRadius(圆角大小)--圆角
 return drawable
end

item={
 LinearLayout;
 layout_height="48dp";
 layout_width="fill";
 {
 LinearLayout;
 layout_height="fill";
 layout_width="fill";
 layout_margin="5dp";
 BackgroundDrawable=布局边框(4,0x77000000,0x00000000,7);
 {
 TextView;
 id="name";
 layout_height="fill";
 layout_width="fill";
 gravity="center";
 textColor="#FFEC6A5C",--文字颜色
 };
 {
 TextView;
 id="url";
 layout_width="0dp";
 layout_height="0dp";
 },
 }
}

adapter=LuaAdapter(activity,item) --执行代码
list.onItemClick=function(l,v,p,i)
 复制文本(webView.getUrl())
 print("正在载入 "..v.Tag.name.text)
 写入文件(悬浮按钮文件,"游戏菜单")
 写入文件(全屏,"已开启全屏")
 写入文件(UA,塞班)
 写入文件(UA标识,"塞班")
 子页面("全屏",v.Tag.url.text) 
end

list.Adapter=adapter --游戏列表
adapter.add{name="冒险岛",url="https://www.yikm.net/play?id=3175&n=L2Zjcm9tL2R6bXgvVGFrYWhhc2hpIE1laWppbiBubyBCb3VrZW4gU2hpbWEgKEopIFshXS5uZXM=&t=%E5%86%92%E9%99%A9%E5%B2%9B"}

adapter.add{name="魂斗罗",url="https://www.yikm.net/play?id=4137&n=L2Zjcm9tL3NqL0NvbnRyYSAoVSkgWyFdLm5lcw==&t=%E9%AD%82%E6%96%97%E7%BD%97"}

adapter.add{name="坦克大战",url="https://www.yikm.net/play?id=4275&n=L2Zjcm9tL3h5eC9CYXR0bGUgQ2l0eSAoSikgWyFdLm5lcw==&t=%E5%9D%A6%E5%85%8B%E5%A4%A7%E6%88%98"}

adapter.add{name="超级玛丽",url="https://www.yikm.net/play?id=3501&n=L2Zjcm9tL2R6bXgvU3VwZXIgTWFyaW8gQnJvcy4gKFcpIFshXS5uZXM=&t=%E8%B6%85%E7%BA%A7%E9%A9%AC%E9%87%8C%E5%A5%A5"}

adapter.add{name="热血格斗",url="https://www.yikm.net/play?id=4714&n=L2Zjcm9tL3lkYnMvTmVra2V0c3UgS2FrdXRvdSBEZW5zZXRzdSAoSikgWyFdLm5lcw==&t=%E7%83%AD%E8%A1%80%E6%A0%BC%E6%96%97"}

adapter.add{name="雪人兄弟",url="https://www.yikm.net/play?id=4511&n=L2Zjcm9tL3h5eC9Tbm93IEJyb3MuIChKKS5uZXM=&t=%E9%9B%AA%E4%BA%BA%E5%85%84%E5%BC%9F"}

adapter.add{name="神庙逃亡",url="http://play.7724.com/olgames/mdtw/?from=wap"}

adapter.add{name="西游记",url="https://www.yikm.net/play?id=3486&n=L2Zjcm9tL2R6bXgvU2FpeXV1a2kgV29ybGQgKEopLm5lcw==&t=%E8%A5%BF%E6%B8%B8%E8%AE%B0(%E6%97%A5%E7%89%88)"}

adapter.add{name="热血曲棍神",url="https://www.yikm.net/play?id=4713&n=L2Zjcm9tL3lkYnMvSWtlIElrZSEgTmVra2V0c3UgSG9ja2V5LWJ1IC0gU3ViZXR0ZSBLb3JvbmRlIERhaSBSYW50b3UgKEopIFshXS5uZXM=&t=%E7%83%AD%E8%A1%80%E6%9B%B2%E6%A3%8D%E7%90%83"}

adapter.add{name="中国象棋",url="http://g.51h5.com/wj/xiangqi/"}

adapter.add{name="钢琴大师",url="http://g.51h5.com/ori/4113/piano/?f=1021"}

adapter.add{name="飞机大作战",url="https://www.tool2.cn/Gams/PlaneWars/"}

adapter.add{name="激龟快打",url="https://www.yikm.net/play?id=3882&n=L2Zjcm9tL2dkL1RlZW5hZ2UgTXV0YW50IEhlcm8gVHVydGxlcyAtIFRvdXJuYW1lbnQgRmlnaHRlcnMgKEUpIFshXS5uZXM=&t=%E6%BF%80%E9%BE%9F%E5%BF%AB%E6%89%93"}

adapter.add{name="魂斗罗力量",url="https://www.yikm.net/play?id=4141&n=L2Zjcm9tL3NqL0NvbnRyYSBGb3JjZSAoVSkgWyFdLm5lcw==&t=%E9%AD%82%E6%96%97%E7%BD%97%E5%8A%9B%E9%87%8F"}

adapter.add{name="热血物语",url="https://www.yikm.net/play?id=4717&n=L2Zjcm9tL3lkYnMvUml2ZXIgQ2l0eSBSYW5zb20gKFUpIFshXS5uZXM=&t=%E7%83%AD%E8%A1%80%E7%89%A9%E8%AF%AD(%E7%BE%8E%E7%89%88)"}

adapter.add{name="2048",url="http://txcgb.3vkj.net/game/2048/"}

adapter.add{name="3D魔方小游戏",url="http://txcgb.3vkj.net/game/mofang/"}

adapter.add{name="欢乐斗地主",url="http://hlddz.huanle.qq.com/"}

adapter.add{name="植物大战僵尸",url="https://demo.mycodes.net/youxi/jspvz/"}

adapter.add{name="拳皇2000",url="https://www.yikm.net/play?id=5329&n=a29mMjAwMC56aXA=&t=%E6%8B%B3%E7%9A%872000&ac=4&l=snk-neo-geo"}

adapter.add{name="恐龙快打",url="https://www.yikm.net/play?id=5334&n=ZGluby56aXA=&t=%E6%81%90%E9%BE%99%E5%BF%AB%E6%89%93&ac=4&l=capcom-cps-1"}

adapter.add{name="热血进行曲",url="https://www.yikm.net/play?id=4725&n=L2Zjcm9tL3lkYnMvRG93bnRvd24gLSBOZWtrZXRzdSBLb3VzaGluIEt5b2t1IC0gU29yZXl1a2UgRGFpdW5kb3VrYWkgKEopIFshXS5uZXM=&t=%E7%83%AD%E8%A1%80%E8%BF%9B%E8%A1%8C%E6%9B%B2"}

adapter.add{name="火柴人大乱斗",url="http://h.4399.com/play/212727.htm"}

