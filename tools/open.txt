-- 设置为竖屏模式
activity.setRequestedOrientation(1)

function hexToColor(hex)
 -- 去掉 # 前缀
 hex = hex:gsub("#", "")
 -- 将十六进制转换为数字
 local color = tonumber(hex, 16)
 return color
end

local hexColor = "#ff009688"
local MyColor = hexToColor(hexColor)
写入文件(随机色,MyColor)
读取颜色(随机色,随机色)

function 网页直达()
 -----分割线-----
 zdy_layout={
 LinearLayout;
 orientation="vertical";--重力属性
 id="zdy_father_layout",
 {
 EditText;--编辑框控件
 id="pathedit",
 layout_marginTop='15dp';--布局顶距
 layout_width="80%w";--编辑框宽度
 layout_gravity="center";--重力属性
 Hint='请输入网页链接';--设置编辑框为空时的提示文字
 };
 };
 对话框()
 .设置标题("网页直达")--设置标题
 .setCancelable(true)--点击外围可取消
 .setView(loadlayout(zdy_layout))--设置布局
 .设置积极按钮("确认",function(v)--设置积极按钮
 if pathedit.text:find ('://') then
 子页面('安卓',pathedit.text)
 else
 网页直达()
 print ('你输入的网址有误')
end
 end)
 .设置消极按钮("取消",function(v)--设置消极按钮
 end)
 .显示()
 ----分割线-----
end


layout = {
 LinearLayout,
 orientation ='vertical',
 layout_width = 'fill',
 layout_height = 'fill',
 background = '#ffffff',
 {
 TextView,
 text = '我的主页',
 textSize = '20sp',
 textColor = '#333333',
 layout_gravity = "center|bottom",
 layout_marginTop = '35px',
 },
 {
 TextView,
 layout_width = '97.4%w',
 layout_height = '5px',
 layout_marginTop = '10dp',
 layout_marginBottom = '10dp',
 layout_gravity = 'center',
 backgroundColor = MyColor
 },
 {
 GridView,
 id = "list",
 numColumns = 3,
 layout_width = 'fill',
 layout_height = 'fill',
 horizontalSpacing = "3dp",
 verticalSpacing = "0dp",
 layout_gravity = "center",
 OverScrollMode = 2
 }
}

webView.addView(loadlayout(layout))

function 布局边框(边框粗细, 边框颜色, 背景颜色, 圆角大小)
 import "android.graphics.drawable.GradientDrawable"
 drawable = GradientDrawable()
 drawable.setShape(GradientDrawable.RECTANGLE)
 drawable.setStroke(边框粗细, tonumber(边框颜色))
 drawable.setColor(tonumber(背景颜色))
 drawable.setCornerRadius(圆角大小)
 return drawable
end

item = {
 LinearLayout,
 layout_height = "48dp",
 layout_width = "fill",
 {
 LinearLayout,
 layout_height = "fill",
 layout_width = "fill",
 layout_margin = "5dp",
 BackgroundDrawable = 布局边框(4,MyColor, 0x00000000, 15),
 {
 TextView,
 id = "name",
 layout_height = "fill",
 layout_width = "fill",
 gravity = "center",
 textColor = "#FF000000"
 },
 {
 TextView,
 id = "tools",
 layout_width = "0dp",
 layout_height = "0dp"
 }
 }
}

adapter = LuaAdapter(activity, item)

收藏小程序=FILES.."tools/shoucang.txt"
if 文件是否存在(收藏小程序)==false then
 写入文件(收藏小程序,收藏夹头文件)
end

function getToolsText(v)
 return v.Tag.tools and v.Tag.tools.text or ""
end

list.onItemClick = function(l, v, p, i)
 local toolsText = getToolsText(v)
 if toolsText:find '://' then
 --print("正在加载\n".. v.Tag.name.text)
 子页面('安卓',toolsText)
 elseif toolsText =='自有频道' then
 密码打开网页('535934D1537AD2E6B7ED663A727EFB4EF1B27CA2A818D86697CD6983FCDA645AF72C7B83C3')
 elseif toolsText =='网页直达' then
 网页直达()
 elseif toolsText =='nil' then
 print ('预留位置',600)
 elseif toolsText =='shoucang' then
 小程序(toolsText)
 elseif toolsText == '启动图片' then
 启动图片设置()
 else
 WEB小程序(toolsText)
 end
end

list.onItemLongClick = function(l, v, p, i)
 local toolsText = getToolsText(v)
 if toolsText =='shoucang' then
 对话框()
 .设置标题('警告！')
 .设置消息('是否确定删除收藏夹，该操作不可恢复')
 .设置积极按钮('确认',function (v)
 写入文件(收藏小程序,收藏夹头文件)
 end)
 .设置消极按钮('取消')
 .显示()
 return true--防止触发点击事件
 else
 --弹出消息 ('暂时没有添加长按功能噢😃！')
 return true
 end
end

list.Adapter = adapter

local dataList = {
 {name = "更多应用", tools = "more"},
 {name = "音乐播放器", tools = "music"},
 {name = "全网搜索", tools = "sousuo"},
 {name = "启动图片", tools = "启动图片"},
 {name = "收藏夹", tools = "shoucang"},
 {name = "历史记录", tools = "file:///storage/emulated/0/txcgb/com.txcgb.free/data/His.html"},
 {name = "常用网站", tools = "changyong"},
 {name = "代码仓库", tools = "daima"},
 {name = "建站资源", tools = "zhan"},
 {name = "应用推荐", tools = "vip"},
 {name = "影视推荐", tools = "ysdq"},
 {name = "休闲游戏", tools = "game"},
 {name = "网页直达", tools = "网页直达"},
 {name = "发布页", tools = "http://cgb0523.ysupan.com/"},
 {name = "预留位置", tools = "nil"},
}

for _, data in ipairs(dataList) do
 adapter.add(data)
end